﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_EF_ConditionMapping.Entity
{
    public class EmployeeEntity
    {
        #region Proeprty
        public int? EmployeeId { get; set; }

        public String FirstName { get; set; }

        public String LastName { get; set; }

        public Boolean IsTerminate { get; set; }
        #endregion 
    }
}
