﻿using Sol_EF_ConditionMapping.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_EF_ConditionMapping
{
    class Program
    {
        static void Main(string[] args)
        {
            Task.Run(async () => {

                IEnumerable<EmployeeEntity> listTerminateEmployeeData =
                    await new EmployeeDal().GetEmployeeTerminateDate();

            }).Wait();
        }
    }
}
